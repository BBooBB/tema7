#include "dspmainwindow.h"
#include "ui_dspmainwindow.h"
#include <math.h>
#include <aquila/global.h>
#include <aquila/transform/FftFactory.h>
#include <aquila/aquila.h>
#include <stdlib.h>
#include <time.h>
#include "aquila/source/generator/TriangleGenerator.h"

DSPMainWindow::DSPMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DSPMainWindow)
{
    ui->setupUi(this);
}
void DSPMainWindow::startClicked()
{
	srand(time(NULL));

	int freq = atoi(ui->lineEdit->text().toStdString().c_str()) * 1000;

	QVector<double>x(2400000);
	for (int i = 0; i < 2400000; i++)
	{
		x[i] = i / 2400000.0;
	}

	QVector<double> c(2400000);
	QVector<double> m(2400000);
	QVector<double> am(2400000);


	Aquila::TriangleGenerator generator(2400000);
	generator.setFrequency(freq/100).setAmplitude(2).generate(2400000 * 0.01);
	for (std::size_t i = 0; i < generator.getSamplesCount(); ++i)
	{
		m[i] = generator.sample(i);
	}

	Aquila::SineGenerator generator2(2400000);
	generator2.setFrequency(freq).setAmplitude(2).generate(2400000 * 0.01);
	for (std::size_t i = 0; i < generator2.getSamplesCount(); ++i)
	{
		c[i] = generator2.sample(i);
	}
	
	for (int i = 0; i < 2400000; i++)
	{
		///*c[i] = 1 * sin(2 * M_PI * freq * x[i]);*/
		// m[i] = 1 * sin(2 * M_PI * (rand()%1100+100) * x[i]);
		am[i] =(1 + m[i])*c[i];
	}


	ui->plot->addGraph();
	ui->plot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->plot->graph(0)->setData(x, am);
	ui->plot->xAxis->setRange(0, 0.01);
	ui->plot->yAxis->setRange(-10, 10);
	ui->plot->replot();

}
DSPMainWindow::~DSPMainWindow()
{
    delete ui;
}
