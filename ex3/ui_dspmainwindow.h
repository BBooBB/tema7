/********************************************************************************
** Form generated from reading UI file 'dspmainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DSPMAINWINDOW_H
#define UI_DSPMAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_DSPMainWindow
{
public:
    QWidget *centralWidget;
    QCustomPlot *plot;
    QLineEdit *lineEdit;
    QPushButton *pushButton;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *DSPMainWindow)
    {
        if (DSPMainWindow->objectName().isEmpty())
            DSPMainWindow->setObjectName(QStringLiteral("DSPMainWindow"));
        DSPMainWindow->resize(1202, 735);
        centralWidget = new QWidget(DSPMainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        plot = new QCustomPlot(centralWidget);
        plot->setObjectName(QStringLiteral("plot"));
        plot->setGeometry(QRect(9, 9, 1180, 601));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(1060, 650, 113, 20));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(960, 650, 75, 23));
        DSPMainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(DSPMainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1202, 21));
        DSPMainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(DSPMainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        DSPMainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(DSPMainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        DSPMainWindow->setStatusBar(statusBar);

        retranslateUi(DSPMainWindow);
        QObject::connect(pushButton, SIGNAL(clicked()), DSPMainWindow, SLOT(startClicked()));

        QMetaObject::connectSlotsByName(DSPMainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *DSPMainWindow)
    {
        DSPMainWindow->setWindowTitle(QApplication::translate("DSPMainWindow", "DSPMainWindow", Q_NULLPTR));
        pushButton->setText(QApplication::translate("DSPMainWindow", "Start", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DSPMainWindow: public Ui_DSPMainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DSPMAINWINDOW_H
