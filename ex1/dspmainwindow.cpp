#include "dspmainwindow.h"
#include "ui_dspmainwindow.h"
#include <math.h>
#include <aquila/global.h>
#include <aquila/transform/FftFactory.h>

DSPMainWindow::DSPMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DSPMainWindow)
{
    ui->setupUi(this);

	
}

DSPMainWindow::~DSPMainWindow()
{
    delete ui;
}
void DSPMainWindow::buttonRezultatClicked()
{

	QVector<double> xAxis(44100);
	for (int i = 0; i < 44100; i++)
	{
		xAxis[i] = i / 44100.0;
	}
	QVector <double> yAxis(44100);
	for (int i = 0; i < 44100; i++)
	{
		yAxis[i] = 1 * sin(2 * M_PI * atoi(ui->lineEdit->text().toStdString().c_str()) * xAxis[i]);
	}
	
	QVector<double>xAxis2(44100);
	for (int i = 0; i < 44100; i++)
	{
		xAxis2[i] = i / 44100.0;
	}
	QVector<double> yAxis2(44100);
	for (int i = 0; i < 44100; i++)
	{
		yAxis2[i] = 1 * sin(2 * M_PI * atoi(ui->lineEdit_2->text().toStdString().c_str()) * xAxis2[i]);
	}
	QVector<double>xAxis3(44100);
	for (int i = 0; i < 44100; i++)
	{
		xAxis3[i] = i / 44100.0;
	}
	QVector<double> yAxis3(44100);
	for (int i = 0; i < 44100; i++)
	{
		yAxis3[i] = 1 * sin(2 * M_PI * atoi(ui->lineEdit_3->text().toStdString().c_str()) * xAxis3[i]);
	}
	QVector<double>xAxis4(44100);
	for (int i = 0; i < 44100; i++)
	{
		xAxis4[i] = i / 44100.0;
	}
	QVector<double> yAxis4(44100);
	for (int i = 0; i < 44100; i++)
	{
		yAxis4[i] = 1 * sin(2 * M_PI * atoi(ui->lineEdit_4->text().toStdString().c_str()) * xAxis4[i]);
	}
	QVector<double>xAxis5(44100);
	for (int i = 0; i < 44100; i++)
	{
		xAxis5[i] = i / 44100.0;
	}
	QVector<double> yAxis5(44100);
	for (int i = 0; i < 44100; i++)
	{
		yAxis5[i] = 1 * sin(2 * M_PI * atoi(ui->lineEdit_5->text().toStdString().c_str()) * xAxis5[i]);
	}
	QVector<double> xAdd(44100), yAdd(44100);
	for (int i = 0; i < 44100; i++)
	{
		xAdd[i] = xAxis[i] + xAxis2[i] + xAxis3[i] + xAxis4[i] + xAxis5[i];
		yAdd[i] = yAxis[i] + yAxis2[i] + yAxis3[i] + yAxis4[i] + yAxis5[i];
	}
	ui->plot->addGraph();
	ui->plot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->plot->graph(0)->setData(xAdd, yAdd);
	ui->plot->xAxis->setRange(0, 0.1);
	ui->plot->yAxis->setRange(-1, 1);
	ui->plot->replot();
}
