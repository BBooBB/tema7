#ifndef DSPMAINWINDOW_H
#define DSPMAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class DSPMainWindow;
}

class DSPMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit DSPMainWindow(QWidget *parent = 0);
    ~DSPMainWindow();
	

private:
    Ui::DSPMainWindow *ui;
	private slots:
		void buttonRezultatClicked();
};

#endif // DSPMAINWINDOW_H
