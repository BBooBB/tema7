#include "dspmainwindow.h"
#include "ui_dspmainwindow.h"
#include <math.h>
#include <aquila/global.h>
#include <aquila/transform/FftFactory.h>

DSPMainWindow::DSPMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DSPMainWindow)
{
    ui->setupUi(this);

	
}

DSPMainWindow::~DSPMainWindow()
{
    delete ui;
}
void DSPMainWindow::buttonRezultatClicked()
{

	QVector<double> xAxis(44100);
	for (int i = 0; i < 44100; i++)
	{
		xAxis[i] = i / 44100.0;
	}
	QVector <double> yAxis(44100);
	for (int i = 0; i < 44100; i++)
	{
		yAxis[i] = 1 * sin(2 * M_PI * atoi(ui->lineEdit->text().toStdString().c_str()) * xAxis[i]);
	}
	
	QVector<double>xAxis2(44100);
	for (int i = 0; i < 44100; i++)
	{
		xAxis2[i] = i / 44100.0;
	}
	QVector<double> yAxis2(44100);
	for (int i = 0; i < 44100; i++)
	{
		yAxis2[i] = 1 * sin(2 * M_PI * atoi(ui->lineEdit_2->text().toStdString().c_str()) * xAxis2[i]);
	}
	QVector<double>xAxis3(44100);
	for (int i = 0; i < 44100; i++)
	{
		xAxis3[i] = i / 44100.0;
	}
	QVector<double> yAxis3(44100);
	for (int i = 0; i < 44100; i++)
	{
		yAxis3[i] = 1 * sin(2 * M_PI * atoi(ui->lineEdit_3->text().toStdString().c_str()) * xAxis3[i]);
	}
	QVector<double>xAxis4(44100);
	for (int i = 0; i < 44100; i++)
	{
		xAxis4[i] = i / 44100.0;
	}
	QVector<double> yAxis4(44100);
	for (int i = 0; i < 44100; i++)
	{
		yAxis4[i] = 1 * sin(2 * M_PI * atoi(ui->lineEdit_4->text().toStdString().c_str()) * xAxis4[i]);
	}
	QVector<double>xAxis5(44100);
	for (int i = 0; i < 44100; i++)
	{
		xAxis5[i] = i / 44100.0;
	}
	QVector<double> yAxis5(44100);
	for (int i = 0; i < 44100; i++)
	{
		yAxis5[i] = 1 * sin(2 * M_PI * atoi(ui->lineEdit_5->text().toStdString().c_str()) * xAxis5[i]);
	}
	QVector<double> xAdd(44100), yAdd(44100);
	for (int i = 0; i < 44100; i++)
	{
		xAdd[i] = xAxis[i] + xAxis2[i] + xAxis3[i] + xAxis4[i] + xAxis5[i];
		yAdd[i] = yAxis[i] + yAxis2[i] + yAxis3[i] + yAxis4[i] + yAxis5[i];
	}
	ui->plot->addGraph();
	ui->plot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->plot->graph(0)->setData(xAdd, yAdd);
	ui->plot->xAxis->setRange(0, 0.1);
	ui->plot->yAxis->setRange(-1, 1);
	ui->plot->replot();

	int FFTsize = 128;
	double frequencyRes = 44100 / FFTsize;
	auto fft = Aquila::FftFactory::getFft(FFTsize);
	auto spectrum = fft->fft(yAdd.toStdVector().data());
	QVector<double> amplitude, frequency;
	int index = 0;
	for each(auto item in spectrum)
	{
		amplitude.push_back(pow(item.real(), 2) + pow(item.imag(), 2));
		frequency.push_back((index++)*frequencyRes);
	}
	ui->plotFrecventa->addGraph();
	ui->plotFrecventa->graph(0)->setLineStyle(QCPGraph::LineStyle::lsImpulse);
	ui->plotFrecventa->graph(0)->setData(frequency, amplitude);
	ui->plotFrecventa->xAxis->setRange(20, 20000);
	ui->plotFrecventa->yAxis->setRange(0, *std::max_element(amplitude.begin(), amplitude.end()));
	ui->plotFrecventa->replot();
}
